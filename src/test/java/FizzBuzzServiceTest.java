import static org.junit.Assert.assertEquals;

import java.util.Collections;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.*;

public class FizzBuzzServiceTest {

	FizzBuzzService service;

	@Mock OracleDao dao;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		
		service = new FizzBuzzService(
				dao,
				new FizzBuzzTranslator()
			);
	}

	@Test
	public void shouldTranslateNumberOfDao() {
		Mockito.when(dao.getInteger()).thenReturn(42);
		
		service.translate();
		
		Mockito.verify(dao).save("Fizz");
	}

}