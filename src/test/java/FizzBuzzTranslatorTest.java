import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class FizzBuzzTranslatorTest {

	FizzBuzzTranslator translator;

	@Before
	public void setUp() {
		translator = new FizzBuzzTranslator();
	}

	@Test
	public void shouldSay1_whenInputIs1() {
		int input = 1;
		String result = translator.translate(input);
		assertEquals("1", result);
	}
	
	@Test
	public void shouldSay2_whenInputIs2() {
		int input = 2;
		String result = translator.translate(input);
		assertEquals("2", result);

	}

	@Test
	public void shouldSayFizz_whenInputIs3() {
		int input = 3;
		String result = translator.translate(input);
		assertEquals("Fizz", result);
	}

	@Test
	public void shouldSayBuzz_whenInputIs5() {
		int input = 5;
		String result = translator.translate(input);
		assertEquals("Buzz", result);
	}

	@Test
	public void shouldSayFizz_whenInputIs6() {
		int input = 6;
		String result = translator.translate(input);
		assertEquals("Fizz", result);
	}

	@Test
	public void shouldSayBuzz_whenInputIs10() {
		int input = 10;
		String result = translator.translate(input);
		assertEquals("Buzz", result);
	}
	
	@Test
	public void shouldSayFizzBuzz_whenInputIs15() {
		int input = 15;
		String result = translator.translate(input);
		assertEquals("FizzBuzz", result);
	}
	
	@Test
	public void shouldSayZero_whenInputIs0() {
		int input = 0;
		String result = translator.translate(input);
		assertEquals("0", result);
	}
}
