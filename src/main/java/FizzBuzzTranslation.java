
public class FizzBuzzTranslation implements Translation {

	private FizzTranslation fizz;
	private BuzzTranslation buzz;

	public FizzBuzzTranslation(){
		fizz = new FizzTranslation();
		buzz = new BuzzTranslation();
	}
	
	public boolean knows(int input) {
		return fizz.knows(input) && buzz.knows(input);
	}

	public String translate(int input) {
		return "FizzBuzz";
	}

}
