
public class BuzzTranslation implements Translation {
	
	public boolean knows(int input) {
		return input % 5 == 0;
	}

	public String translate(int input) {
		return "Buzz";
	}
}
