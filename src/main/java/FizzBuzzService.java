import java.util.List;

public class FizzBuzzService {

	private Dao dao;
	private FizzBuzzTranslator translator;

	public FizzBuzzService(Dao dao, FizzBuzzTranslator translator) {
		this.dao = dao;
		this.translator = translator;
	}

	public void translate() {
		Integer input = dao.getInteger();
		String translation = translator.translate(input);
		dao.save(translation);
	}
}
