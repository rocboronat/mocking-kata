
public interface Translation {

	boolean knows(int input);

	String translate(int input);

}
