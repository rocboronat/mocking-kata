public class FizzTranslation implements Translation {

	public boolean knows(int input) {
		return input % 3 == 0;
	}

	public String translate(int input) {
		return "Fizz";
	}

}
