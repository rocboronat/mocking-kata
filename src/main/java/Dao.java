import java.util.List;

public interface Dao {

	void save(String translation);

	int getInteger();

}
