import java.util.ArrayList;
import java.util.List;

public class FizzBuzzTranslator {
	static List<Translation> translations = new ArrayList();
	static {
		translations.add(new FizzBuzzTranslation());
		translations.add(new FizzTranslation());
		translations.add(new BuzzTranslation());
		translations.add(new CommonTranslation());
	}

	public String translate(int input) {
		if (input == 0) {
			return "0";
		}
		for (Translation translation : translations) {
			if (translation.knows(input)) {
				return translation.translate(input);
			}
		}
		throw new RuntimeException("translate can't handle this value");
	}

}
